# -*- coding: utf-8 -*-
import logging
import json
from taskhandlers import handlers
import config
import time
import requests
from hashlib import sha256
import os
#import handlers
logging.basicConfig(format='%(asctime)s->%(levelname)s:[in %(filename)s:%(lineno)d]:%(message)s'
    , level=int(config.APP_LOGLEVEL)
)

SIGNER_STRING = 'signer.v1'
PRIVATE_KEY = ''
SIGNER_ADDRESS = ''

def generate_key_file(web3, fname, passw):
    try:
        acc = web3.create_account()
        encrypted = acc.encrypt(passw)
        with open(fname, 'w') as acc_file:
            acc_file.write(json.dumps(encrypted))
        logging.info('New account: {}'.format(encrypted))
    except Exception as e:
        logging.error("Error! Can't create account: {}".format(e))

def init_signer(web3):
    global SIGNER_ADDRESS, PRIVATE_KEY
    logging.info('Trying to init signer for {}'.format(config.KEY_FILE_SECRET_NAME))

    ret = 1
    try:
        with open('/run/secrets/{}'.format(config.KEY_FILE_SECRET_NAME)) as pass_secret:
            SIGNER_KEY_PASSW = pass_secret.read()
            logging.info('Password:{}'.format(SIGNER_KEY_PASSW))

        if not os.path.exists(config.KEY_FILE):
            logging.info('Key not exist, trying to create: {}'.format(config.KEY_FILE))
            generate_key_file(web3, config.KEY_FILE, SIGNER_KEY_PASSW)
            ret = 0

        with open(config.KEY_FILE) as keyfile:
            encrypted_key = keyfile.read()
            logging.info('Encrypted_key: {}'.format(encrypted_key))
            PRIVATE_KEY = web3.key_decrypt(encrypted_key, SIGNER_KEY_PASSW)
            SIGNER_ADDRESS = '0x{}'.format(eval(encrypted_key)['address'])
            logging.info('Signer: {}'.format(SIGNER_ADDRESS))

    except Exception as e:
        logging.error("Error! Can't init signer: {}".format(e))
        ret = -1

    return ret


def sign_timed():
    key_active = config.APP_KEY_ACTIVE
    app_name = config.APP_NAME
    app_id = config.APP_ID
    app_key = config.APP_KEY
    now = int(time.time())
    time_block = str(int(now / int(key_active)))
    
    return app_id + '.' + sha256(f'{app_name}{app_key}{time_block}'.encode('utf-8')).hexdigest()

def dummy():
    logging.info('Unknown task type, start dummy handler')
    return { 'error': 'unknown task type' }

def get_handler(task_type):
    handler = {
        0: handlers.get_proof_of_freez,
        1: handlers.get_proof_of_burn
    }.get(task_type, dummy)
    return handler

def register_signer(base_url):
    url = base_url + '/signer/register'
    auth = sign_timed()
    logging.info('Register signer, auth: {}'.format(auth))
    data = {
        'signer_address': SIGNER_ADDRESS,
        'signer_version': SIGNER_STRING}
    try:
        resp = requests.post(url, headers={'Content-Type': 'application/json',
            'Authorization': auth},
            json = data)
        logging.info('RESPONSE BODY: {}'.format(resp.json()))
    except Exception as e:
        logging.error('RESPONSE ERROR: {}'.format(e))
        return False

    return True


def get_tasks(base_url):
    logging.info('Try to get tasks...')
    url = base_url + '/signer/tasks'
    auth = sign_timed()
    logging.info('Get tasks, auth: {}'.format(auth))
    data = {
        'signer_address': SIGNER_ADDRESS,
        'signer_version': SIGNER_STRING}
    try:
        resp = requests.get(url, headers={'Content-Type': 'application/json',
            'Authorization': auth},
            json = data)
    except Exception as e:
        logging.error('Error! Get tasks returned: {}'.format(e))
        return (None, '{}'.format(e))
    
    try:
        data = json.loads(str(resp.json()))
        if 'error' in data:
            logging.info('API returned error: {}'.format(data['error']))
            return (None, data)
        if 'info' in data:
            logging.info('API returned: {}'.format(data['info']))
            return (None, data)
        if not 'task_id' in data or not 'task_type' in data or not 'task_data' in data or not 'task_params' in data:
            logging.info('Error! Received unknown data, signer update may be needed (DATA: {})'.format(data))
            return (None, data)
    except Exception as e:
        logging.error('Error! Some problems with response: {}'.format(resp.json()))
        return (None, {'error': 'some problems with response: {}'.format(resp.json())})
    return (data, None)

def send_proof(base_url, data):
    url = base_url + '/signer/proof'
    auth = sign_timed()
#    logging.info('Send proof, auth: {}'.format(auth))
    data['signer_info'] = {
        'signer_address': SIGNER_ADDRESS,
        'signer_version': SIGNER_STRING}
    try:
        resp = requests.post(url, headers={'Content-Type': 'application/json',
            'Authorization': auth},
            json = data)
#        logging.info('RESPONSE BODY: {}'.format(resp.json()))
    except Exception as e:
        logging.error('Error! Can\'t send proof: {}'.format(e))
        return False
    return True

def get_task_types(base_url):
    logging.info('Try to get task types...')
    url = base_url + '/signer/get_task_types'
    auth = sign_timed()
    try:
        resp = requests.get(url, headers={'Content-Type': 'application/json',
                'Authorization': auth})
    except Exception as e:
        logging.error('Error! Get task types returned: {}'.format(e))
    try:
        data = json.loads(str(resp.json()))
        for d in data:
            logging.info('Task type_id: {} name: {}'.format(d['task_id'], d['name']))
    except Exception as e:
        logging.error('Error! Some problems with response: {}'.format(resp.json()))
        return {'error': 'some problems with response: {}'.format(resp.json())}

    return data

def main():
    base_url = config.BASE_URL
    providers = {}

    # 1. Init at least one provider
    providers = handlers.init_providers(config.PROVIDERS_FILE)

    if len(providers) <= 0:
        logging.error('Error! Providers not initialized')
        return
    
    logging.info('NUMBER OF PROVIDERS: {}\n{}'.format(len(providers), providers))
    
    # 2. Select any provider for key init
    web3 = providers[str(list(providers.keys())[0])]
    ret = init_signer(web3)
    if ret < 0:
        return

    if ret == 0: # new key created, try to register
        register_signer(base_url)
        return

    # 3. Get list of task types (test)
    get_task_types(base_url)

    # 4. Get task from api
    while True:
        ret = get_tasks(base_url)
        logging.debug('TASK DATA: {}'.format(ret))
        if ret[0] == None:
        # signer key exists, but not registered
            if 'error' in ret[1] and ret[1]['error'] == 'signer not registered':
                register_signer(base_url) # try to register
                return
            if 'error' in ret[1]:
                logging.error('An error occurred while receiving data: {}'.format(ret[1]))
                return
            if 'info' in ret[1] and ret[1]['info'] == 'no tasks':
                logging.info('No tasks, wait {} sec'.format(config.NEW_CHECK_TIMEOUT))
                time.sleep(config.NEW_CHECK_TIMEOUT)
                continue
        # get task data
        data = ret[0]
        logging.info('Got new task, id={}'.format(data['task_id']))
        logging.info('>>> chain id: {}'.format(data['task_data'][0]['chain_id']))
        logging.info('>>> tx hash: {}'.format(data['task_data'][0]['tx_hash']))
        logging.info('>>> task type: {}'.format(data['task_type']))
        logging.debug('>>> task data: {}'.format(data['task_data']))
        logging.debug('>>> task params: {}'.format(data['task_params']))
        task_type = int(data['task_type'])

    # 5. Select appropriate handler for current task
        handler = get_handler(task_type)

    # 6. Create message and sign
        ret = handler(providers, SIGNER_ADDRESS, PRIVATE_KEY, data)
#        logging.info('GET PROOF: {}'.format(ret))
        if 'error' in ret:
            logging.error('Error! {}'.format(ret['error']))
            return

    # 7. Send proof to api
        ret = send_proof(base_url, ret)
        if ret == True:
            logging.info('Proof sended')
        logging.info('Wait {} sec'.format(config.NEW_CHECK_TIMEOUT))
        time.sleep(config.NEW_CHECK_TIMEOUT)

    return

###########################################
if __name__ == '__main__':
    main()
