# -*- coding: utf-8 -*-
import logging
import os
from web3 import Web3, HTTPProvider, IPCProvider, WebsocketProvider, middleware, exceptions
from web3.gas_strategies.time_based import medium_gas_price_strategy
from eth_account.messages import encode_defunct
#from eth_abi import encode_single
from eth_abi import encode

APP_LOGLEVEL = os.environ.get('APP_LOGLEVEL', 20)

class Web3Worker:
    def __init__(self, chain_id, provider):
        self.chain_id = 0
        logging.basicConfig(format='%(asctime)s->%(levelname)s:[in %(filename)s:%(lineno)d]:%(message)s'
            , level=int(APP_LOGLEVEL)
        )
        logging.info('chain={}, provider={}'.format(chain_id, provider))
        #web3 provider initializing
        if 'https:'.upper() in provider.upper():
            self.w3 = Web3(HTTPProvider(provider))
        elif 'ws:'.upper() in provider.upper() or 'wss:'.upper() in provider.upper():
            self.w3 = Web3(Web3.WebsocketProvider(provider))
        else:
            self.w3 = Web3(IPCProvider(provider))
        logging.info('w3.api={}, w3.net.version={}, w3.eth.block_number={}, provider={}'.format(self.w3.api, self.w3.net.version, self.w3.eth.block_number, provider))

        #Need some injection on Rinkeby and -dev networks
        if self.w3.eth.chain_id in [4, 5, 56, 137]:
            from web3.middleware import geth_poa_middleware
            self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self.chain_id = chain_id

    def key_decrypt(self, enc, passw):
        return self.w3.eth.account.decrypt(enc, passw)

    def key_encrypt(self, key, passw):
        return self.w3.eth.account.encrypt(key, passw)
    
    def create_account(self):
        return self.w3.eth.account.create()
    
    def get_tx_receipt(self, tx_hash):
        return self.w3.eth.get_transaction_receipt(tx_hash)
    
    def encode_message(self, types, msg):
        return encode(types, msg)
    
    def sign_encoded_message(self, enc_msg, priv_key):

        logging.debug('Encoded message: {}'.format(enc_msg))
        hashed_msg = Web3.solidity_keccak(['bytes32'], [enc_msg])
        message = encode_defunct(primitive=hashed_msg)
        signed_message = self.w3.eth.account.sign_message(message, private_key=priv_key)

        return signed_message.signature.hex()
    
    def verify_encoded_message(self, enc_msg, signature):
        hashed_msg = Web3.solidity_keccak(['bytes32'], [enc_msg])
        message = encode_defunct(primitive=hashed_msg)

        return self.w3.eth.account.recover_message(message, signature=signature).lower()
