import logging
import os
import json
from . import web3worker

APP_LOGLEVEL = os.environ.get('APP_LOGLEVEL', 20)

def init_providers(providers_fname):
    providers = {}
    try:
        with open(providers_fname) as providers_file:
            provs = json.loads(providers_file.read())
            logging.info('Init providers:')
            for x in provs:
                logging.info('chain: {}, provider: {}'.format(x['chain_id'], x['provider']))
                providers[x['chain_id']] = web3worker.Web3Worker(int(x['chain_id']), x['provider'])
    except Exception as e:
        logging.error("{}(): Error! Can't init providers: {}".format(init_providers.__name__, e))
    return providers


def get_proof_of_freez(providers, signer_address, priv_key, data):
    """
    Event topic:
    NFTKeeper.topics:
    'NewFreeze': "0x6c565b64c33e2e003e67f1483722b55db3e4fe744580c81b5922e38a0534cc18"
    """
    chain_id = data['task_data'][0]['chain_id']
    tx_hash = data['task_data'][0]['tx_hash']
    if not str(chain_id) in providers:
        logging.info('Error! Unsupported chain: {}, check providers file.'.format(chain_id))
        return {'error': 'unsupported chain: {}'.format(chain_id)}
    web3 = providers[str(chain_id)]

    valid_topics = [
        x['valid_topic']
        for x in data['task_params']
    ]
    keepers = [
        x['valid_keeper'][str(chain_id)]
        for x in data['task_params']
    ]

    logging.info('{}(): Try to sign: chain={} tx_hash={} keepers={}'.format(get_proof_of_freez.__name__, web3.chain_id, tx_hash, keepers))
    logging.debug('TOPICS: {}'.format(valid_topics))
    logging.debug('KEEPERS: {}'.format(keepers))

    try:
        receipt = web3.get_tx_receipt(tx_hash)
    except Exception as e:
        logging.error('{}(): Error! Can\'t get tx receipt for {}: {}'.format(get_proof_of_freez.__name__, tx_hash, e))
        return { 'error': e.args[0] }

    in_data = {}
    in_data['source_chain'] = web3.chain_id
    in_data['data_type'] = '(address,uint256[],address[],uint256[])'
    in_data['sender'] = receipt['from']
    in_data['tx_hash'] = receipt.transactionHash.hex()

    in_data['source_keeper'] = []
    in_data['target_chain'] = [] # int
    in_data['target_contract'] = []
    in_data['target_token_id'] = [] # int

    for x in receipt.logs:
        if x.topics[0].hex().lower() in valid_topics and \
            x.address.lower() in keepers:
            in_data['source_keeper'].append(x.address.lower())
            in_data['target_chain'].append(int(x.topics[1].hex(), 0))
            in_data['target_contract'].append('0x' + x.topics[2].hex()[26:])
            in_data['target_token_id'].append(int(x.topics[3].hex(), 0))

    msg = {}
    msg['oracle_signer'] = signer_address
    msg['task_id'] = data['task_id']
    msg['oracle_signature'] = None
    msg['encoded_message'] = None

    num = len(in_data['source_keeper'])
    if num > 0:
        try:
            encoded_message = web3.encode_message(
                [in_data['data_type']],
                [(in_data['sender'], in_data['target_chain'], in_data['target_contract'], in_data['target_token_id'])]
            )
            msg['encoded_message'] = '0x' + encoded_message.hex()
            msg['oracle_signature'] = web3.sign_encoded_message(encoded_message, priv_key)
            logging.debug('{}(): encoded_message={}'.format(get_proof_of_freez.__name__, msg['encoded_message']))
            logging.info('{}(): Ok, signed, signature={}'.format(get_proof_of_freez.__name__, msg['oracle_signature']))
        except Exception as e:
            logging.error('{}(): Error! {}'.format(get_proof_of_freez.__name__, e))
            return { 'error': e.args[0] }
    else:
        logging.error('{}(): Error! Message len={}, proof not found: chain_id={} tx_hash={}'.format(get_proof_of_freez.__name__, num, web3.chain_id, tx_hash))
        return { 'error': 'proof not found' }

    # test verify
    try:
        ver = web3.verify_encoded_message(msg['encoded_message'], msg['oracle_signature'])
        if ver == signer_address:
            logging.info('{}(): verification result: {} - ok'.format(get_proof_of_freez.__name__, ver))
        else:
            logging.error('{}(): Error! Mismatch result: {}, signer_address: {}'.format(get_proof_of_freez.__name__, ver, signer_address))
            return { 'error': 'mismatch result' }
    except Exception as e:
        logging.error('{}(): Error! Can\'t verify signature: {}'.format(get_proof_of_freez.__name__, e))
        return { 'error':'verify = ' + e.args[0] }

    return msg




    try:
        receipt = web3.get_tx_receipt(tx_hash)
        msg = [
            {'source_chain': web3.chain_id,
             'source_keeper': x.address.lower(),
             'target_chain': int(x.topics[1].hex(), 0),
             'target_contract': '0x' + x.topics[2].hex()[26:],
             'target_token_id': int(x.topics[3].hex(), 0),
             'sender': receipt['from'],
             'tx_hash': receipt.transactionHash.hex(),
             'oracle_signer': signer_address,
             'data_type': '(address,uint256,address,uint256)',
             'task_id': data['task_id'],
             'oracle_signature': None
            }
            for x in receipt.logs
#            if x.topics[0].hex().lower() == valid_topics and x.address.lower() == keepers
            if x.topics[0].hex().lower() in valid_topics and x.address.lower() in keepers
        ]
        if len(msg) == 1:
            r = msg[0]
            r['oracle_signature'] = web3.prepare_and_sign_freez_proof(
                r['data_type'],
                (r['sender'], r['target_chain'], r['target_contract'], r['target_token_id']),
                priv_key).signature.hex()
            logging.info('{}(): Ok, signed, signature={}'.format(get_proof_of_freez.__name__, r['oracle_signature']))
        else:
            logging.info('{}(): Error! Message len={}, proof not found: chain_id={} tx_hash={}'.format(get_proof_of_freez.__name__, len(msg), web3.chain_id, tx_hash))
            r = {'error': 'Proof not found'}
    except Exception as e:
        logging.error("{}(): Error! Can't get tx receipt for {}: {}".format(get_proof_of_freez.__name__, tx_hash, e))
        return {'error': e.args[0]}
    return r

def get_proof_of_burn(providers, signer_address, priv_key, data):
    """
    Event topic:
    Spawner721.topics:
    'Transfer': "0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef"
    """
    chain_id = data['task_data'][0]['chain_id']
    tx_hash = data['task_data'][0]['tx_hash']
    if not str(chain_id) in providers:
        logging.error('{}(): Error! Unsupported chain: {}, check providers file.'.format(get_proof_of_burn.__name__, chain_id))
        return { 'error': 'unsupported chain: {}'.format(chain_id) }
    web3 = providers[str(chain_id)]

    valid_topics = [
        x['valid_topic']
        for x in data['task_params']
    ]
    logging.info('{}(): Try to sign: chain={} tx_hash={}'.format(get_proof_of_burn.__name__, web3.chain_id, tx_hash))
    logging.debug('TOPICS: {}'.format(valid_topics))
    try:
        receipt = web3.get_tx_receipt(tx_hash)
    except Exception as e:
        logging.error('{}(): Error! Can\'t get tx receipt for {}: {}'.format(get_proof_of_burn.__name__, tx_hash, e))
        return { 'error': e.args[0] }

    in_data = {}
    in_data['data_type'] = '(address,address[],uint256[])'
    in_data['burned_contract'] = []
    in_data['target_contract'] = []
    in_data['target_token_id'] = []
    in_data['sender'] = receipt['from']
    for x in receipt.logs:
        if x.topics[0].hex().lower() in valid_topics and \
            int(x.topics[2].hex(), 0) == 0 and \
            len(x.topics) == 4: # EIP721
            in_data['burned_contract'].append(x.address.lower())
            in_data['target_contract'].append('0x'+x.topics[2].hex()[26:])
            in_data['target_token_id'].append(int(x.topics[3].hex(), 0))
    msg = {}
    msg['oracle_signer'] = signer_address
    msg['task_id'] = data['task_id']
    msg['oracle_signature'] = None
    msg['encoded_message'] = None

    num = len(in_data['burned_contract'])
    if num > 0:
        try:
            encoded_message = web3.encode_message(
                [in_data['data_type']],
                [(in_data['sender'], in_data['burned_contract'], in_data['target_token_id'])]
            )
            msg['encoded_message'] = '0x' + encoded_message.hex()
            msg['oracle_signature'] = web3.sign_encoded_message(encoded_message, priv_key)
            logging.debug('{}(): encoded_message={}'.format(get_proof_of_burn.__name__, msg['encoded_message']))
            logging.info('{}(): Ok, signed, signature={}'.format(get_proof_of_burn.__name__, msg['oracle_signature']))
        except Exception as e:
            logging.error('{}(): Error! {}'.format(get_proof_of_burn.__name__, e))
            return { 'error': e.args[0] }
    else:
        logging.error('{}(): Error! Message len={}, proof not found: chain_id={} tx_hash={}'.format(get_proof_of_burn.__name__, num, web3.chain_id, tx_hash))
        return { 'error': 'proof not found' }

    # test verify
    try:
        ver = web3.verify_encoded_message(msg['encoded_message'], msg['oracle_signature'])
        if ver == signer_address:
            logging.info('{}(): verification result: {} - ok'.format(get_proof_of_burn.__name__, ver))
        else:
            logging.error('{}(): Error! Mismatch result: {}, signer_address: {}'.format(get_proof_of_burn.__name__, ver, signer_address))
            return { 'error': 'mismatch result' }
    except Exception as e:
        logging.error('{}(): Error! Can\'t verify signature: {}'.format(get_proof_of_burn.__name__, e))
        return { 'error':'verify = ' + e.args[0] }

    return msg
