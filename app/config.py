# -*- coding: utf-8 -*-
import os
import json

APP_LOGLEVEL = os.environ.get('APP_LOGLEVEL',10)# logging.DEBUG-10 .....logging.CRITICAL-50
API_KEY = os.environ.get('API_KEY','')

KEY_FILE_SECRET_NAME = os.environ.get('KEY_FILE_SECRET_NAME')
KEY_FILE = os.environ.get('KEY_FILE')
PROVIDERS_FILE = os.environ.get('PROVIDERS_FILE')
APP_NAME = os.environ.get('APP_NAME', '')
APP_ID = os.environ.get('APP_ID', '')
APP_KEY = os.environ.get('APP_KEY', '')
APP_KEY_ACTIVE = os.environ.get('APP_KEY_ACTIVE', '')
BASE_URL = os.environ.get('BASE_URL', '')
NEW_CHECK_TIMEOUT = int(os.environ.get('NEW_CHECK_TIMEOUT', 30))
